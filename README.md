# Sophie Text Formation Utility (STFU)

A python library that built to make formatting easier.

You can read the changelog [here](NOTES.md)


## A simple example


#### A one-liner

```python
from stfu import Section, KeyValue

print(Section(
    KeyValue("This is example key", "Example value"),
    "Example string",
    title="Example title"
))
```


#### Support for adding new items after creating a object

```python
from stfu import Section, KeyValue

sec = Section(title="Example title")
sec += KeyValue("This is example key", "Example value")
sec += "Example string"
print(sec)
```


#### The result of both snippets
```html
<b><u>Example title</u></b>:
  - <b>This is example key</b>: Example value
  - Example string
```
![img_1.png](images/img_1.png)